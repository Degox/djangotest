"""untitled1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from RESTExample import views
from django.contrib import admin
urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^/?$', name='index', view=views.index),
    url(r'^human/$', name='humans', view=views.humans),
    url(r'^human/(?P<id>\d+)/$', name='human', view=views.human),
    # url(r'^add', name='add', view=views.add),
    # url(r'^remove/(?P<id>\d+)/$', name='remove', view=views.remove),
]

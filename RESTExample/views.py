import sys

from django.http import QueryDict
from django.shortcuts import render, redirect

from RESTExample.models import Human


# Create your views here.
def humans(request):
    _humans = Human.objects.all()
    context = {'humans': _humans, 'title': 'Люди'}
    if request.method == 'GET':
        pass
    elif request.method == 'PUT':
        _human = Human()
        data = QueryDict(request.body)
        print(data)
        for param in data.keys():
            if param != 'csrfmiddlewaretoken':
                setattr(_human, param, data.get(param))
        _human.save()
    return render(request, 'list.html', context)


def human(request, id):
    _human = Human.objects.filter(id=id).first()
    if request.method == 'GET':
        context = {'human': _human, 'title': 'Редактирование'}
        return render(request, 'edit.html', context)
    elif request.method == 'DELETE':
        _human = Human.objects.filter(id=id)
        _human.delete()
    elif request.method == 'POST':
        for param in request.POST:
            if param != 'csrfmiddlewaretoken':
                setattr(_human, param, request.POST[param])
        _human.save()
    return redirect(humans)

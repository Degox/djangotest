from django.db import models


# Create your models here.
class Human(models.Model):
    id = models.AutoField(primary_key=True)
    birthDate = models.DateField()
    firstName = models.CharField(max_length=50)
    lastName = models.CharField(max_length=50)
    age = models.IntegerField(null=False)
